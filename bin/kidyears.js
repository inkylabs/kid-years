#!/usr/bin/env node
import delay from 'delay'
import { promises as fs } from 'fs'
import minimist from 'minimist'
import os from 'os'

import kidyears from '../index.js'

async function main () {
  const argv = minimist(process.argv.slice(2))
  const configpath = (argv._[0] || `${os.homedir}/.kidyears.json`)
  let data
  try {
    data = await fs.readFile(configpath)
  } catch (e) {
    console.error(`config "${configpath}" could not be read: ${e}`)
    return 1
  }
  let config
  try {
    config = JSON.parse(data)
  } catch (e) {
    console.error(`config "${configpath}" could not be parsed: ${e}`)
    return 1
  }

  const digits = parseInt(argv.d || 2)
  while (true) {
    const num = kidyears.calculate(config).toFixed(digits)
    console.log(`${num}\r`)
    await delay(100)
    process.stdout.moveCursor(0, -1)
    process.stdout.clearLine(1)
  }
}

main().then(process.exit, console.error)
