/* eslint-env jest */
import kidyears from '.'

const KIDS = {
  maybe: {
    start: '2011-03-06'
  },
  ford: {
    start: '2012-09-02'
  },
  karis: {
    start: '2014-01-21'
  },
  flick: {
    start: '2015-07-17'
  },
  deacon: {
    start: '2017-01-01'
  },
  ritz: {
    start: '2018-11-27'
  },
  sam: {
    start: '2020-07-04'
  }
}

describe('calculate', () => {
  [
    ['one kid', [KIDS.maybe], '2022-01-01', 10.82],
    ['7 kids', Object.values(KIDS), '2022-01-01', 44.15]
  ]
    .forEach(([name, input, end, ex]) => {
      it(name, () => {
        expect(kidyears.calculate(input, { end })).toBeCloseTo(ex, 2)
      })
    })
})
