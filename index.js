import { DateTime } from 'luxon'

function calculate (kids, opts) {
  opts = Object.assign({
    end: DateTime.now()
  }, opts)
  if (typeof opts.end === 'string' || opts.end instanceof String) {
    opts.end = DateTime.fromISO(opts.end)
  }

  let total = 0
  for (const k of kids) {
    const start = DateTime.fromISO(k.start)
    let end = opts.end
    if (k.end) end = DateTime.fromISO(k.end)
    const diff = end.diff(start, ['years', 'milliseconds'])
    const nextAnniversary = start.plus({ years: diff.years + 1 })
    const remainingMs = nextAnniversary.diff(end, 'milliseconds')
    const partialYear = diff.milliseconds / (diff.milliseconds + remainingMs)
    total += diff.years + partialYear
  }
  return total
}

export default {
  calculate
}
